package ui;

import java.text.ParseException;
import java.util.InputMismatchException;
import java.util.Scanner;
import java.util.concurrent.TimeUnit;

import app.Employee;
import app.OperationNotAllowedException;
import app.PersonalActivity;
import app.Project;
import app.ProjectActivity;
import app.ProjectApp;

/**
 * @author Peer-review Iacob & Victor
 */

public class UserInterface {
	
//
	public static void main(String[] args) {
		int projectnr = 0;
		Project project = null;
		ProjectActivity activity = null;
		PersonalActivity personalactivity = null;
		Employee employee = null;
		ProjectActivity projectactivity = null;
		System.out.println("Please Enter your initials");
		ProjectApp projectapp = new ProjectApp();
		String userid = "";
		Employee user = null;
		String intent = null;
		Scanner in = new Scanner(System.in);

		
		// first logon
		while (!(user != null)) {
			try {
				userid = in.next();
				user = new Employee(userid);
				projectapp.registerEmployee(user);
			} catch (ParseException e) {
				System.out.println(e);
			}
		}

		// the main "screen"

		while (true) {
			in = new Scanner(System.in);
			//user options
			System.out.println("What do you want to do?");
			System.out.println("Press 1 if you wish to create a new project");
			System.out.println("Press 2 if you wish to create an activity on your project");
			System.out.println("Press 3 if you wish to create a personal activity");
			System.out.println("Press 4 if you wish to change the project leader on your project");
			System.out.println("Press 5 if you wish to register time spent");
			System.out.println("Press 6 if you wish to add project members to your activity");
			System.out.println("Press 7 if you wish to invite an employee to your project");
			System.out.println("Press 8 if you wish to become leader of a project your are a member of");
			System.out.println("Press 10 if you wis to check the projects of an employee");
			System.out.println("Press 0 if you wish to quit");
			// admin use:
			System.out.println("Press 9 if you wish to add an employee to the firm (test/admin functionality)");
			try {
				intent = in.next();
			} catch (Exception e) {
	

			}

			if (intent.equals("1")) {
				System.out.println(
						"Please input these parameters in order as seperate lines: Name(can be anything), start date (in the form yyyy-mm-dd) and end date(same as start)");
				String projectname = in.next();
				String projectStart = in.next();
				String projectEnd = in.next();
				try {
					project = new Project(projectname);
					project.setStartDate(projectStart);
					project.setStartDate(projectEnd);
					projectapp.addProject(project);
					System.out.println("your project has been created with the project number:" + project.getProjectNumber());
					project.addProjectMember(user);

					try {	//delay to allow the user to see confirmation if it worked
						TimeUnit.MILLISECONDS.sleep(1800);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				} catch (Exception e1) {
					System.out.println(e1);
				}

			}

			// new activity on project
			if (intent.equals("2")) {
				try {
				System.out.println("First enter the project id of the project you wish to add an activity to");
				projectnr = in.nextInt();
				}catch(InputMismatchException e) {
					System.out.println(e);
				}
				try {
					project = projectapp.searchProject(projectnr);
					System.out.println(
							"Please input these parameters in order as seperate lines: Name(can be anything), start date (in the form yyyy-mm-dd) and end date(same as start)");
					String Activityname = in.next();
					String ActivityStart = in.next();
					String ActivityEnd = in.next();
					activity = new ProjectActivity(project, Activityname);
					activity.setStartDate(ActivityStart);
					activity.setEndDate(ActivityEnd);
					project.addProjectActivity(activity);
					System.out.println("your activity has been created");
					try {
						TimeUnit.MILLISECONDS.sleep(1800);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				} catch (Exception e2) {
					System.out.println(e2);
				}
				
			}
			// new personal activity
			if (intent.equals("3")) {

				try {
					System.out.println(
							"Please input these parameters in order as seperate lines: Name(can be anything), start date (in the form yyyy-mm-dd) and end date(same as start)");
					String PActivityname = in.next();
					String PActivityStart = in.next();
					String PActivityEnd = in.next();
			personalactivity = new PersonalActivity(PActivityname);
					personalactivity.setStartDate(PActivityStart);
					personalactivity.setEndDate(PActivityEnd);
					System.out.println("your personal activity has been created");

					try {
						TimeUnit.MILLISECONDS.sleep(1800);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				} catch (Exception e1) {
					System.out.println(e1);
				}

			}

			// changing project leader
			if (intent.equals("4")) {
				try {
					project.isprojectleader(false);
					System.out.println("First enter the project id of the project you wish to change leader on");
					projectnr = in.nextInt();
					System.out.println(project.getEmployees());
					project = projectapp.searchProject(projectnr);
					try {
						if (project.getProjectLeaderID().equals(userid)) {
							project.isprojectleader(true);
						}
					} catch (Exception e) {
					}
					System.out.println("Who is going to be the chosen one? (****)");
					String employeename = in.next();
					employee = projectapp.searchEmployee(employeename);
					project.setProjectLeader(employee);
					System.out.println("new leader has been chosen (one)");
					try {
						TimeUnit.MILLISECONDS.sleep(1800);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				} catch (OperationNotAllowedException e1) {
					// TODO Auto-generated catch block
					System.out.println(e1);
				}

			}
			if (intent.equals("5")) {

				try {
					System.out.println("First enter the project id of the project you wishes to add time to");
					projectnr = in.nextInt();
					project = projectapp.searchProject(projectnr);
					System.out.println("Enter the name of the activity you wish to add time to");
					String Activityname = in.next();
					projectactivity = project.searchProjectActivities(Activityname);
					System.out.println("Please enter the amount of hours worked so far as a double (0.0)");
					double hourspent = in.nextDouble();
					projectactivity.registerCumulatedTime(hourspent);
					System.out.println("Hours has been added to the overall project and activity");
					try {
						TimeUnit.MILLISECONDS.sleep(1800);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}

				} catch (OperationNotAllowedException e1) {
					// TODO Auto-generated catch block
					System.out.println(e1);
				}

			}
			if (intent.equals("6")) {
				System.out.println("First enter the project id you wish to work on");
				try {
					System.out.println("Then enter the activity name you wish to add an employee to");
					String activityname = in.next();
					projectactivity = project.searchProjectActivities(activityname);
					System.out.println("now enter the employees initials");

					String employeename = in.next();
					employee = projectapp.searchEmployee(employeename);
					project.addProjectMember(employee);
					System.out.println("The employee has been added");
					try {
						TimeUnit.MILLISECONDS.sleep(1800);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}

				} catch (OperationNotAllowedException e1) {
					System.out.println(e1);

				}

			}
			if (intent.equals("7")) {
				try {
					projectnr = 0;
					System.out.println("First enter the project id you wish to add an employee to");
					try {
					projectnr = in.nextInt();
					}
					catch(InputMismatchException e) {
						
					}
					project = projectapp.searchProject(projectnr);
					System.out.println("now enter the employees initials");

					String employeename = in.next();
					employee = projectapp.searchEmployee(employeename);
					project.addProjectMember(employee);
					System.out.println("The employee has been added");
					try {
						TimeUnit.MILLISECONDS.sleep(1800);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}

				} catch (OperationNotAllowedException e1) {
					System.out.println(e1);

				}

			}

			if (intent.equals("8")) {
				try {
					System.out.println("First enter the project id you wants to be leader of");
					projectnr = in.nextInt();
					project = projectapp.searchProject(projectnr);
					project.setProjectLeader(user);
					System.out.println(
							"you are now the choosen one for this project");
					try {
						TimeUnit.MILLISECONDS.sleep(1800);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				} catch (Exception e1) {
					// TODO Auto-generated catch block
					System.out.println(e1);
				}

			}
			if (intent.equals("9")) {
				try {
					System.out.println("Enter employee initials (****)");
					String employeename = in.next();
					Employee employee1 = new Employee(employeename);
					projectapp.registerEmployee(employee1);
					System.out.println("The employee has been added to the firm");
					try {
						TimeUnit.MILLISECONDS.sleep(1800);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				} catch (ParseException e) {
					System.out.println(e);
				}

			}
			if (intent.equals("0")) {
				System.out.println("goodbye");
				System.exit(0);
				in.close();
			}
			if (intent.equals("10")) {
				try {
					System.out.println("First enter the initials of the employee you wish to check");
					String employeename = in.next();
					System.out.println(projectapp.searchEmployee(employeename).getAssignedProjectinfo());
					try {
						TimeUnit.MILLISECONDS.sleep(1800);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}

				} catch (OperationNotAllowedException e1) {
					// TODO Auto-generated catch block
					System.out.println(e1);
				}

			}else {
				System.out.println("No command found");
			}

		}
	}
}
