package app;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
/**
 * @author Frederik
 */
public abstract class TimePlanning {

	public Date startDate = new Date();
	public Date endDate = new Date();
	public double timeSpent = 0;
	public double allocatedTime = 0;

	public void setStartDate(String newStartDateString) throws ParseException {
		// Dates has to be parsed in the following format. Example "2011-01-01"
		try {
			Date sf = new SimpleDateFormat("yyyy-MM-dd").parse(newStartDateString);
			this.startDate = sf;

		} catch (ParseException e) {
			throw new ParseException("Cannot parse dates", 0);
		}
	}

	public void setEndDate(String newEndDateString) throws ParseException {
		// Dates has to be parsed in the following format. Example "2011-01-01"
		try {
			Date sf = new SimpleDateFormat("yyyy-MM-dd").parse(newEndDateString);
			this.endDate = sf;
		} catch (ParseException e) {
			throw new ParseException("Cannot parse dates", 0);
		}
	}
	public void setAllocatedTime(double value) {
		this.allocatedTime = value;
	}

	public String getStartDate() { // Outputs in the format "Start date: dd month, yyyy"
		String text = String.format("%1$s %2$td %2$tB, %2$tY", "Start date:", this.startDate);
		return text;
	}

	public String getEndDate() {// Outputs in the format "End date: dd month, yyyy"
		String text = String.format("%1$s %2$td %2$tB, %2$tY", "End date:", this.endDate);
		return text;
	}
	
	public double getAllocatedTime() {
		return this.allocatedTime;
	}

	public double getCumulatedTime() {
		return this.timeSpent;
	}

}
