package app;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Iacob
 */

public class ProjectApp {
	public List<Project> projects = new ArrayList<>();
	public List<Employee> employees = new ArrayList<>();

	public void addProject(Project project) {
		projects.add(project);
	}

	public void registerEmployee(Employee employee) {
		employees.add(employee);
	}

	public List<Project> getProjects() {
		return projects;
	}

	public Project searchProject(int searchText) throws OperationNotAllowedException { //uses a given project number to return a specific project as an object
		Project foundProject = null;
		for (Project p : projects) {
			if (p.getProjectNumber() == searchText) {
				foundProject = p;
			}
		}
		if (foundProject == null) {
			throw new OperationNotAllowedException("Project doesn't exists");

		} else {

			return foundProject;
		}
	}

	public Employee searchEmployee(String searchText) throws OperationNotAllowedException { //similar to above, but with employee ID
		Employee foundEmployee = null;
		for (Employee e : employees) {
			if (e.getID().contains(searchText)) {
				foundEmployee = e;
			}
		}
		if (foundEmployee == null) {
			throw new OperationNotAllowedException("Cannot find employee");
		}
		return foundEmployee;

	}

}