package app;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import app.TimePlanning;

/**
 * @author Thomas
 */

public class Project extends TimePlanning {

	public String projectName;
//
	public int year = Calendar.getInstance().get(Calendar.YEAR);
	public static int runningNumber = 1;  //increments every created project object
	public int projectNumber = year * 10000 + runningNumber;  //starts at 20180001
	public boolean UserIsProjectLeader = false; 

	public String projectLeaderID;
	public Employee projectLeader;
	public List<ProjectActivity> projectActivities = new ArrayList<>();
	public List<Employee> assignedEmployees = new ArrayList<>();

	public Project(String projectName) throws ParseException {
		if (projectName.isEmpty()) {
			throw new ParseException("Cannot parse name", 0);

		} else {
			this.projectName = projectName;
			runningNumber++;
		}
	}

	public void addProjectActivity(ProjectActivity projectActivity) throws OperationNotAllowedException {
		projectActivities.add(projectActivity);
	}

	public void addProjectMember(Employee employee) throws OperationNotAllowedException {
		if (assignedEmployees.contains(employee)) {
			throw new OperationNotAllowedException("Employee is already a Project Member");
		}
		assignedEmployees.add(employee);
		employee.addProject(this);
	}

	public double getTotalHoursSpent() {  //sums projectactivities registered time
		double hoursSpent = 0;
		for (ProjectActivity pA : projectActivities) {
			hoursSpent = +pA.getCumulatedTime();
		}

		return hoursSpent;
	}

	public void setProjectLeader(Employee employee) throws OperationNotAllowedException {  
		if (assignedEmployees.contains(employee)) { //first invoked employee has to be in the project
			if (this.projectLeaderID == null || UserIsProjectLeader == true) {  //if there's either no leader already or the user is the leader
				this.projectLeaderID = employee.getID();
				this.projectLeader = employee;
			} else {
				throw new OperationNotAllowedException("There already is a leader");
			}

		} else {
			throw new OperationNotAllowedException("cannot make an outside leader");
		}
	}

	public Employee searchEmployee(String employeeID) throws OperationNotAllowedException, ParseException { //similar to what is in projectapp, but for project members
		Employee foundEmployee =null;
		for (Employee e : assignedEmployees) {
			if (e.getID().contains(employeeID)) {
				foundEmployee = e;
			}
		}

		if (foundEmployee == null) {
			throw new OperationNotAllowedException("Cannot find employee");
		} else {
			return foundEmployee;
		}
	}

	public String getProjectName() {
		return this.projectName;
	}

	public String getProjectLeaderID() {
		return this.projectLeaderID;
	}

	public int getProjectNumber() {
		return this.projectNumber;
	}

	public List<ProjectActivity> getActivities() {
		return projectActivities;
	}

	public List<Employee> getEmployees() {
		return assignedEmployees;
	}

	public void isprojectleader(boolean b) { //flag checked by the ui
		UserIsProjectLeader = b;
	}

	public ProjectActivity searchProjectActivities(String searchText) throws OperationNotAllowedException { 
		ProjectActivity activity = null;
		for (ProjectActivity e : projectActivities) {
			if (e.getActivityName().contains(searchText)) {
				activity = e;
			}
		}
		if (activity == null) {
			throw new OperationNotAllowedException("Cannot find activity");
		}
		return activity;

	}

}
