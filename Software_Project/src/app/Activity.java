
package app;

import java.text.ParseException;

/**
 * @author Victor
 */

public abstract class Activity extends TimePlanning {

	public String activityName;

	public Activity(String name) throws ParseException {
		if (name.isEmpty()){
			throw new ParseException("Cannot parse name", 0);
		} else {
			this.activityName = name;

		}
	}

	public String getActivityName() {
		return this.activityName;
	}
	
}
