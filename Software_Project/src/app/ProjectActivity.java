package app;


import java.util.ArrayList;
import java.util.List;

/**
 * @author Victor
 */

public class ProjectActivity extends Activity {


	public int associatedProjectID;
	public List<Employee> assignedEmployees = new ArrayList<>();
	public Project project;


	public ProjectActivity(Project project, String name) throws Exception {
		super(name);
		this.project = project;

	}

	public void addProjectMember(Employee employee) throws OperationNotAllowedException {
		if (project.getEmployees().contains(employee)) { //check whether invoked employee is on the project's member list
			assignedEmployees.add(employee);
			employee.addProjectActivity(this);
		} else {
			throw new OperationNotAllowedException("cannot add outsiders to a project activity");
		}
	}


	public List<Employee> getAssignedEmployees() {
		return this.assignedEmployees;
	}

	public void registerCumulatedTime(double hours) {
		this.timeSpent += hours;
	}


}
