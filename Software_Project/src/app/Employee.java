package app;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Frederik
 */

public class Employee {

	public String employeeID;
	public List<PersonalActivity> currentPersonalActivities = new ArrayList<>();
	public List<ProjectActivity> currentProjectActivities = new ArrayList<>();
	public List<Project> assignedProjects = new ArrayList<>();
	public List<Employee> employees = new ArrayList<>();

	public Employee(String employeeID) throws ParseException {

		int numberofinitials = 4;
		if (!(numberofinitials == employeeID.length())) {
			throw new ParseException("Cannot parse name", 0);
		} else
			this.employeeID = employeeID;
	}

	public void addPersonalActivity(PersonalActivity personalActivity) {
		currentPersonalActivities.add(personalActivity);
	}

	public void addProjectActivity(ProjectActivity projectActivity) {
		currentProjectActivities.add(projectActivity);
	}

	public void addProject(Project project) {
		assignedProjects.add(project);
	}

	public String getID() {
		return this.employeeID;
	}

	public List<PersonalActivity> getPersonalActivities() {
		return currentPersonalActivities;
	}

	public List<ProjectActivity> getProjectActivities() {
		return currentProjectActivities;
	}

	public List<String> getAssignedProjectinfo() {// Loops through a smart for-loop for every Project object, adds the
													// projects project to an arraylist and returns it
		// return assignedProjects;
		List<String> info = new ArrayList<>();
		for (Project p : assignedProjects) {

			info.add(p.getProjectName());
			info.add(String.valueOf(p.getProjectNumber()));

		}
		return info;
	}
}
