Feature: Project leader creates an activity 
	Description: The project leader is part of a 
	Actor: Project Leader
	
Scenario: Successfully creates activity and mans it
	Given project exists with name "project_management_program", start "2011-01-01", end "2011-01-01".
	Given Creates activity with name "activity_project", start "2011-01-01", end "2011-01-01".
	Given set allocated time to 20 hours
	When I add the activity
 	Then Activity with the name, "activity_project" , start date of "Start date: 01 January, 2011" and "End date: 01 January, 2011" is added to the list
	Then Allocated time is 20

Scenario: Successfully creates activity and fails to man it
	Given project exists with name "project_management_program", start "2011-01-01", end "2011-01-01".
	Given Creates activity with name "activity_project", start "2011-01-01", end "2011-01-01".
	Given Wants to assign an employee with the info "LALA"
	When The activity is created
	Then The user is given error message "cannot add outsiders to a project activity"
	
Scenario: Failed to create activity
	Given project exists with name "project_management_program", start "2011-01-01", end "2011-01-01".
	When Failed to create activity with name "DICKs", start "", end ""
	Then User is given the error message: "Cannot parse dates" 
