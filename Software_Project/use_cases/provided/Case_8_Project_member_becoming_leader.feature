Feature: Project member becoming leader
	Description: A project member on a given project can promote himself to be the project leader if there is not already a project leader
	Actor: Project member
	
Scenario: Project member successfully becomes a leader
	Given project exists with name "project_management_program", start "2011-01-01", end "2011-01-01".
	Given Wants to assign a project member with the initials "BUZZ".	
	When The project member is promoted 
	Then The employee with the set of "BUZZ" is changed to be the leader of the project  
	
Scenario: Project member fails to become leader due to there already being a leader
	Given project exists with name "project_management_program", start "2011-01-01", end "2011-01-01".
	Given Wants to assign a project member with the initials "BUZZ".
	Given project member "ZUBB"	is leader
	When The project member is promoted 
	Then The user is given error message "There already is a leader"
	
Scenario: Project member fails to become leader due to not being on the project member list
	Given project exists with name "project_management_program", start "2011-01-01", end "2011-01-01".
	Given Wants to assign an employee with the info "TOTO"
	When The project member is promoted 
	Then The user is given error message "cannot make an outside leader"