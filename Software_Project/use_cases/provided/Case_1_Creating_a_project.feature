Feature: Creating a project 
	Description: An employee creates a new project with a set of parameters and optionally a name
	Actors: Employee

Scenario: Create a project successfullythe 
	When Create project with name "project_management_program", start "2011-01-01", end "2011-01-01". 
	Then Project with the name, "project_management_program" , start date of "Start date: 01 January, 2011" and "End date: 01 January, 2011" is added to the list 
	
	
Scenario: fail to create project due to wrong dates 

	When Failed to creat project with name "random_project", start "", end "" 
	When Create project with name "random_project", start "", end "". 
	Then User is given the error message: "Cannot parse dates" 
	
Scenario: fail to create project due to wrong name 
	When Failed to creat project with name "", start "2011-10-11", end "2012-06-01" 
	Then User is given the error message: "Cannot parse name" 