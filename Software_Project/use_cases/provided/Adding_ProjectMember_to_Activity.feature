Feature: Project leader mans an activity 
	Description: The project leader mans project members to an activity
	Actor: Project Leader
	
Scenario: Successfully mans activity
	Given project exists with name "project_management_program", start "2011-01-01", end "2011-01-01".
	Given activity exists with name "activity_project", start "2011-01-01", end "2011-01-01".
	Given a project member with the initials "JHLN"
	When Wants to assign a project member with the initials "JHLN" to the activity with the name "activity project".
 	Then THe project member's list has the activity'
	
Scenario: fails to man
	Given project exists with name "project_management_program", start "2011-01-01", end "2011-01-01".
	Given activity exists with name "activity_project", start "2011-01-01", end "2011-01-01".
	Given a project member with the initials "LALA"
	When Wants to assign a project member with the initials "JHLN" to the activity with the name "activity project".
	Then The user is given error message "Cannot find employee"
	
	