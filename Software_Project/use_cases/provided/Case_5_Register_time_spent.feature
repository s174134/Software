Feature: Employee registers time spent on an activity 
Description: An employee at the end of the day registers his time spent.
			 If he's a part of the given project/activity it is registered normally and registered as outside help if not 
Actors: User who can be any kind of employee

Scenario: User is a normal member of the activity and project
	Given project exists with name "project_management_program", start "2011-01-01", end "2011-01-01".
	Given project number is 20180001	
	Given activity exists with name "activity_project", start "2011-01-01", end "2011-01-01".
	And User is an employee with initials "BULI" who is a part of the project and activity
	When the user registers 5 hours time spent on the project with project number 20180001 and project activity "activity_project" 
	Then time spent 5 is added to the activity and overall project
 
