Feature: inviting another employee to a project
	Description: Any member of a project can invite his employees, but cannot invite a person already part of the project
	Actor: Project member

Scenario:  Successfully adding an employee to a project 
	Given project exists with name "project_management_program", start "2011-01-01", end "2011-01-01".
	Given Wants to assign an employee with the info "RAND"
	When the employee is found with string "RAND" added to project 
	Then the employee with the initials "RAND" is in the project's employee list

Scenario:  Getting an error by inviting an employee who is already part of the project
	Given project exists with name "project_management_program", start "2011-01-01", end "2011-01-01".
	Given Wants to assign an employee with the info "DNAR"
	Given "DNAR" is already contained in the project 
	When the employee is found with string "DNAR" added to project 
	Then The user is given error message "Employee is already a Project Member"
