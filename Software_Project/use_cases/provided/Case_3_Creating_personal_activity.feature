Feature: Employee creates a personal activity 
	Description: Any employee can create a personal activity for vacation or other personal leaves, 
	cannot invite or add members and are special activities in that they are not attached to a project.
	Has a name field that serves an informal information to the leaders
	Actor: Employee

Scenario: Successfully create the activity 
	Given Employee with the info "LALA" exist
	When creates a personal activity with the name "sick_leave" which starts "2011-01-01" and ends "2011-01-01"
	Then the personal activity personal activity list with name"sick_leave" "Start date: 01 January, 2011" and "End date: 01 January, 2011"

Scenario: Fail to create the activity 
	Given Employee with the info "LALA" exist
	When creates a personal activity with the name "" which starts "2011-01-01" and ends "2011-01-01"	
	Then User is given the error message: "Cannot parse name" 

Scenario: Create activity without calling startDate
	Given Employee with the info "LALA" exist
	Given Wishes to create a personal activity with the name "WIll_be_gone_for_a_week" which starts today and ends "2018-05-010"
	When the the activity is created
	Then the personal activity is added to LALA'' personal activity list with  name"WIll_be_gone_for_a_week"and "End date: 10 May, 2018"

	