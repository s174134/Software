 Feature: Change new project leader 
Description: The current project leader wants to step down and promotes another project member, the potential leader
Actors: Project Leader

Scenario: Successfully change project leader
	Given project exists with name "project_management_program", start "2011-01-01", end "2011-01-01".
	Given User is leader of project
	Given a project member with the initials "LALA"
	When The project member is promoted 
	Then The employee with the set of "LALA" is changed to be the leader of the project  

Scenario: Fails to change project leader due to the new potential leader not being on the project
	Given project exists with name "project_management_program", start "2011-01-01", end "2011-01-01".
	Given Wants to assign an employee with the info "heji"
	When The project member is promoted 
	Then The user is given error message "cannot make an outside leader"
