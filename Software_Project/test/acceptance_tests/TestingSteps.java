package acceptance_tests;

import static org.hamcrest.CoreMatchers.*;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.hasItem;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import app.Employee;
import app.OperationNotAllowedException;
import app.PersonalActivity;
import app.Project;
import app.ProjectActivity;
import app.ProjectApp;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
public class TestingSteps {
	/** merged from smaller files to avoid dependency injection issues different people wrote each cases despite being in the same fail
	 * 
	 * @author Victor: case 1 and 2
	 * @author Thomas: case 3 and 4
	 * @author Iacob: case 5 and 6, and bonus adding to activity
	 * @author Frederik: case 7 and 8

	 * 
	 */
	private ProjectApp projectapp;
	public Project project;
	private ProjectActivity projectActivity;
	private Employee employee;
	private Object errormessage;
	private PersonalActivity personalActivity;
	private Employee employeelead;
	private Project projectfound;
	List<String> foundinfo = new ArrayList<>();
	// Case 1

	public TestingSteps(ProjectApp projectapp) {
		this.projectapp = projectapp;
	}

	@Given("^project exists with name \"([^\"]*)\", start \"([^\"]*)\", end \"([^\"]*)\"\\.$")
	public void createProjectWithNameStartEnd(String name, String start, String end) throws Exception {
		project = new Project(name);
		project.setStartDate(start);
		project.setEndDate(end);
		projectapp.addProject(project);

	}

	@When("^Create project with name \"([^\"]*)\", start \"([^\"]*)\", end \"([^\"]*)\"\\.$")
	public void createProjectWithNameStartEnd1(String name, String start, String end) throws Exception {
		try {
			project = new Project(name);
			project.setStartDate(start);
			project.setEndDate(end);
			projectapp.addProject(project);
		} catch (ParseException e) {
			errormessage = e.getMessage();
		}

	}

	@Then("^Project with the name, \"([^\"]*)\" , start date of \"([^\"]*)\" and \"([^\"]*)\" is added to the list$")
	public void projectWithTheNameStartDateOfAndIsAddedToTheList(String arg1, String arg2, String arg3)
			throws Exception {
		assertThat(project.getProjectName(), is(equalTo(arg1)));
		assertThat(project.getStartDate(), is(equalTo(arg2)));
		assertThat(project.getEndDate(), is(equalTo(arg3)));
		assertThat(projectapp.getProjects(), hasItem(project));

	}

	@Then("^User is given the error message: \"([^\"]*)\"$")
	public void userIsGivenTheErrorMessage(String arg1) throws Exception {
		assertEquals(arg1, errormessage);

	}

	// Case 1 scenario 2
	@When("^Failed to creat project with name \"([^\"]*)\", start \"([^\"]*)\", end \"([^\"]*)\"$")
	public void failedToCreatProjectWithNameStartEnd(String name, String start, String end) throws Exception {
		try {
			project = new Project(name);
			project.setStartDate(start);
			project.setEndDate(end);
			projectapp.addProject(project);
		} catch (ParseException e) {
			errormessage = e.getMessage();
		}
	}

	@When("^Failed to create activity with name \"([^\"]*)\", start \"([^\"]*)\", end \"([^\"]*)\"$")
	public void failedToCreateActivityWithNameStartEnd(String name, String startDate, String endDate) throws Exception {
		try {
			projectActivity = new ProjectActivity(project, name);
			projectActivity.setStartDate(startDate);
			projectActivity.setEndDate(endDate);
		} catch (ParseException e) {
			errormessage = e.getMessage();
		}
	}

	// Case 2

	@Given("^Creates activity with name \"([^\"]*)\", start \"([^\"]*)\", end \"([^\"]*)\"\\.$")
	public void LeaderCreatsActivityInProject(String name, String startDate, String endDate) throws Exception {
		projectActivity = new ProjectActivity(project, name);
		projectActivity.setAllocatedTime(10);
		projectActivity.setStartDate(startDate);
		projectActivity.setEndDate(endDate);
	}

	@Given("^activity exists with name \"([^\"]*)\", start \"([^\"]*)\", end \"([^\"]*)\"\\.$")
	public void activityExistsWithNameStartEnd(String arg1, String arg2, String arg3) throws Exception {
		projectActivity = new ProjectActivity(project, arg1);
		projectActivity.setStartDate(arg2);
		projectActivity.setEndDate(arg3);
		project.addProjectActivity(projectActivity);
	}

	@When("^I add the activity$")
	public void iAddTheActivity() throws Exception {
		project.addProjectActivity(projectActivity);
	}

	@Given("^set allocated time to (\\d+) hours$")
	public void setAllocatedTimeToHours(int arg1) throws Exception {
		projectActivity.setAllocatedTime(arg1);
	}

	@Then("^Allocated time is (\\d+)$")
	public void allocatedTimeIs(double arg1) throws Exception {
		assertThat(projectActivity.getAllocatedTime(), is(equalTo(arg1)));

	}

	@Given("^Project member with the info \"([^\"]*)\" exist$")
	public void projectMemberWithTheInfoExist(String arg1) throws Exception {
		employee = new Employee(arg1);
		projectapp.registerEmployee(employee);
		project.addProjectMember(employee);
	}
 
	@When("^The employee gets assigned project information for \"([^\"]*)\"$")
	public void theEmployeeGetsAssignedProjectInformationFor(String arg1) throws Exception {
		// Write code here that turns the phrase above into concrete actions
		foundinfo = projectapp.searchEmployee(arg1).getAssignedProjectinfo();
	}

	@Then("^the employee gets the project name and  project number$")
	public void theEmployeeGetsTheProjectNameAndProjectNumber() throws Exception {
		assertThat(foundinfo, hasItem(project.getProjectName()));

	}

	@When("^Wants to assign a project member with the initials \"([^\"]*)\" to the activity with the name \"([^\"]*)\"\\.$")
	public void wantsToAssignAProjectMemberWithTheInitialsToTheActivityWithTheName(String arg1, String arg2)
			throws Exception {
		try {
			projectActivity.addProjectMember(project.searchEmployee(arg1));
		} catch (Exception e) {
			errormessage = e.getMessage();
		}
	}

	@Then("^Activity with the name, \"([^\"]*)\" , start date of \"([^\"]*)\" and \"([^\"]*)\" is added to the list$")
	public void activityWithTheNameStartDateOfAndIsAddedToTheList(String arg1, String arg2, String arg3)
			throws Exception {
		assertThat(projectActivity.getActivityName(), is(equalTo(arg1)));
		assertThat(projectActivity.getStartDate(), is(equalTo(arg2)));
		assertThat(projectActivity.getEndDate(), is(equalTo(arg3)));
		assertThat(project.getActivities(), hasItem(projectActivity));
	}

	@Then("^THe project member's list has the activity'$")
	public void theProjectMemberSListHasTheActivity() throws Exception {
		assertThat(projectActivity.getAssignedEmployees(), hasItem(employee));
		assertThat(employee.getProjectActivities(), hasItem(projectActivity));
	}

	@Given("^Wants to assign a project member with the initials \"([^\"]*)\"\\.$")
	public void wantsToAssignAProjectMemberWithTheInitials(String arg1) throws Exception {
		employee = new Employee(arg1);
		project.addProjectMember(employee);
	}

	@When("^The activity is created$")
	public void theActivityIsCreated() throws Exception {
		try {
			project.addProjectActivity(projectActivity);
			projectActivity.addProjectMember(employee);
		} catch (OperationNotAllowedException e) {
			errormessage = e.getMessage();
		}
	}

	@Given("^Wants to assign an employee with the info \"([^\"]*)\"$")
	public void wantsToAssignAnEmployeeWithTheInfo(String arg1) throws Exception {
		employee = new Employee(arg1);
		projectapp.registerEmployee(employee);
	}

	//
	@Then("^The user is given error message \"([^\"]*)\"$")
	public void theUserIsGivenErrorMessage(String arg1) throws Exception {
		assertEquals(arg1, errormessage);
	}

	// case 3

	@Given("^Employee with the info \"([^\"]*)\" exist$")
	public void employeeWithTheInfoExist(String employeeID) throws Exception {
		employee = new Employee(employeeID);
		projectapp.registerEmployee(employee);
	}

	@When("^creates a personal activity with the name \"([^\"]*)\" which starts \"([^\"]*)\" and ends \"([^\"]*)\"$")
	public void createsAPersonalActivityWithTheNameWhichStartsAndEnds(String arg1, String arg2, String arg3)
			throws Exception {
		try {
			personalActivity = new PersonalActivity(arg1);
			personalActivity.setStartDate(arg2);
			personalActivity.setEndDate(arg3);
			employee.addPersonalActivity(personalActivity);
		} catch (Exception e) {
			errormessage = e.getMessage();

		}
	}

	@When("^the the activity is created$")
	public void theTheActivityIsCreated() throws Exception {
		employee.addPersonalActivity(personalActivity);
	}

	@Given("^Wishes to create a personal activity with the name \"([^\"]*)\" which starts today and ends \"([^\"]*)\"$")
	public void wishesToCreateAPersonalActivityWithTheNameWhichStartsTodayAndEnds(String arg1, String arg2)
			throws Exception {
		personalActivity = new PersonalActivity(arg1);
		personalActivity.setEndDate(arg2);
	}

	@Then("^the personal activity personal activity list with name\"([^\"]*)\" \"([^\"]*)\" and \"([^\"]*)\"$")
	public void thePersonalActivityPersonalActivityListWithNameAnd(String arg1, String arg2, String arg3)
			throws Exception {
		assertThat(employee.getPersonalActivities(), hasItem(personalActivity));
		assertThat(personalActivity.getActivityName(), is(equalTo(arg1)));
		assertThat(personalActivity.getStartDate(), is(equalTo(arg2)));
		assertThat(personalActivity.getEndDate(), is(equalTo(arg3)));
	}

	@Then("^the personal activity is added to LALA'' personal activity list with  name\"([^\"]*)\"and \"([^\"]*)\"$")
	public void thePersonalActivityIsAddedToLALAPersonalActivityListWithNameAnd(String arg1, String arg2)
			throws Exception {
		assertThat(employee.getPersonalActivities(), hasItem(personalActivity));
		assertThat(personalActivity.getActivityName(), is(equalTo(arg1)));
		assertThat(personalActivity.getEndDate(), is(equalTo(arg2)));
	}

	// case 4.

	@Given("^User is leader of project$")
	public void userIsLeaderOfProject() throws Exception {
		project.isprojectleader(true);

	}

	@Given("^a project member with the initials \"([^\"]*)\"$")
	public void aProjectMemberWithTheInitials(String ID) throws Exception {
		employee = new Employee(ID);
		projectapp.registerEmployee(employee);
		project.addProjectMember(employee);

	}

	@When("^The project member is promoted$")
	public void theProjectMemberIsPromoted() throws Exception {
		try {
			project.setProjectLeader(employee);

		} catch (OperationNotAllowedException e) {

			errormessage = e.getMessage();
		}

	}

	@Then("^The employee with the set of \"([^\"]*)\" is changed to be the leader of the project$")
	public void theEmployeeWithTheSetOfIsChangedToBeTheLeaderOfTheProject(String employeeID) throws Exception {
		assertEquals(employeeID, project.getProjectLeaderID());

	}

	// case 5

	@Given("^User is an employee with initials \"([^\"]*)\" who is a part of the project and activity$")
	public void userIsAnEmployeeWithInitialsWhoIsAPartOfTheProjectAndActivity(String init) throws Exception {
		employee = new Employee(init);
		project.addProjectMember(employee);
		projectActivity.addProjectMember(employee);
	}

	@When("^the user registers (\\d+) hours time spent on the project with project number (\\d+) and project activity \"([^\"]*)\"$")
	public void theUserRegistersHoursTimeSpentOnTheProjectWithProjectNumberAndProjectActivity(int arg1, int arg2,
			String arg3) throws Exception {
		projectapp.searchProject(arg2).searchProjectActivities(arg3).registerCumulatedTime(arg1);
		;

	}

	@Then("^time spent (\\d+) is added to the activity and overall project$")
	public void timeSpentIsAddedToTheActivityAndOverallProject(double hours) throws Exception {
		double epsilon = 1; // epsilon describes the precision of the assertEquals.
		assertEquals(hours, project.getTotalHoursSpent(), epsilon);
		assertEquals(hours, projectActivity.getCumulatedTime(), epsilon);
	}
	// case 6

	@Given("^project number is (\\d+)$")
	public void projectNumberIs(int number) throws Exception {
		project.projectNumber = number;

	}

	// case 7

	@When("^the employee is found with string \"([^\"]*)\" added to project$")
	public void theEmployeeIsFoundWithStringAddedToProject(String arg1) throws Exception {
		try {
			project.addProjectMember(projectapp.searchEmployee(arg1));
		} catch (OperationNotAllowedException e) {
			errormessage = e.getMessage();
		}
	}

	@Then("^the employee with the initials \"([^\"]*)\" is in the project's employee list$")
	public void theEmployeeWithTheInitialsIsInTheProjectSEmployeeList(String arg1) throws Exception {
		assertThat(project.getEmployees(), hasItem(employee));
	}

	@Given("^\"([^\"]*)\" is already contained in the project$")
	public void isAlreadyContainedInTheProject(String arg1) throws Exception {
		project.addProjectMember(employee);
	}

	// case 8

	@Given("^project member \"([^\"]*)\"	is leader$")
	public void projectMemberIsLeader(String arg1) throws Exception {
		employeelead = new Employee(arg1);
		project.addProjectMember(employeelead);
		project.setProjectLeader(employeelead);
		project.UserIsProjectLeader = false;
	}
}
