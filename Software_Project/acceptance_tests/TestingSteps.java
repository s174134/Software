package acceptance_tests;

import static org.hamcrest.CoreMatchers.*;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.hasItem;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import app.Employee;
import app.OperationNotAllowedException;
import app.PersonalActivity;
import app.Project;
import app.ProjectActivity;
import app.ProjectApp;
import cucumber.api.PendingException;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class TestingSteps {
	private ProjectApp projectapp;
	public Project project;
	private List<Project> projects = new ArrayList<>();
	private ProjectActivity projectActivity;
	private Employee employee;
	private Object errormessage;
	private PersonalActivity personalActivity;
	private Employee employeelead;
	// Case 1

	public TestingSteps(ProjectApp projectapp) {
		this.projectapp = projectapp;
	}

	@Given("^Create project with name \"([^\"]*)\", start \"([^\"]*)\", end \"([^\"]*)\"\\.$")
	public void createProjectWithNameStartEnd(String name, String start, String end) throws Exception {
		project = new Project(name, start, end);
	}

	@When("^project added to lists$")
	public void ProjectIsCreated() throws Exception {
//		try {
			projectapp.addProject(project);
//		} catch (OperationNotAllowedException e) {
//			errormessage = e.getMessage();
//		}

	}

	@Then("^Project with the name, \"([^\"]*)\" , start date of \"([^\"]*)\" and \"([^\"]*)\" is added to the list$")
	public void projectWithTheNameStartDateOfAndIsAddedToTheList(String arg1, String arg2, String arg3)
			throws Exception {
		assertThat(project.getProjectName(), is(equalTo(arg1)));
		assertThat(project.getStartDate(), is(equalTo(arg2)));
		assertThat(project.getEndDate(), is(equalTo(arg3)));
		assertThat(projectapp.getProjects(), hasItem(project));

	}

	// Case 1 scenario 2
	@When("^Failed to creat project with name \"([^\"]*)\", start \"([^\"]*)\", end \"([^\"]*)\"$")
	public void failedToCreatProjectWithNameStartEnd(String name, String start, String end) throws Exception {
		try {
			project = new Project(name, start, end);
			projectapp.addProject(project);
		} catch (OperationNotAllowedException e) {
			errormessage = e.getMessage();
		}
	}

	@Then("^user is given the mmessage that \"([^\"]*)\"$")
	public void giveerrormessageforProject(String error) throws Exception {
			assertEquals(error, errormessage);
	}
	

	// Case 2

	@Given("^Creates activity with name \"([^\"]*)\", start \"([^\"]*)\", end \"([^\"]*)\"\\.$")
	public void LeaderCreatsActivityInProject(String name, String startDate, String endDate) throws Exception {
		// project = new Project(name,startDate,endDate);
		
			projectActivity = new ProjectActivity(project, name, startDate, endDate);

		
	}
//	@Given("^Creates activity with name \"([^\"]*)\" start \"([^\"]*)\" end \"([^\"]*)\"$")
//	public void createsActivityWithNameStartEnd(String arg1, String arg2, String arg3) throws Exception {
//		projectActivity = new ProjectActivity(project, arg1, arg2, arg3);
//	}

	@Then("^Activity with the name, \"([^\"]*)\" , start date of \"([^\"]*)\" and \"([^\"]*)\" is added to the list$")
	public void activityWithTheNameStartDateOfAndIsAddedToTheList(String arg1, String arg2, String arg3) throws Exception {
		assertThat(projectActivity.getActivityName(), is(equalTo(arg1)));
		assertThat(projectActivity.getActivityStart(), is(equalTo(arg2)));
		assertThat(projectActivity.getActivityEnd(), is(equalTo(arg3)));
		assertThat(project.getActivities(), hasItem(projectActivity));
	}




	@Given("^Wants to assign a project member with the initials \"([^\"]*)\"\\.$")
	public void wantsToAssignAProjectMemberWithTheInitials(String arg1) throws Exception {
		employee = new Employee(arg1);
		project.addProjectMember(employee);
	}

	@When("^The activity is created$")
	public void theActivityIsCreated() throws Exception {
		try {
			project.addProjectActivity(projectActivity);
			projectActivity.addProjectMember(employee);
		} catch (OperationNotAllowedException e) {
			// errormessageholder.setErrorMessage(e.getMessage());
			errormessage = e.getMessage();
		}

	}

	@When("^Failed to creat activity with name \"([^\"]*)\", start \"([^\"]*)\", end \"([^\"]*)\"$")
	public void theActivityIsCreated2(String name, String startDate, String endDate) throws Exception {

		try {
			projectActivity = new ProjectActivity(project, name, startDate, endDate);
			project.addProjectActivity(projectActivity);
			projectActivity.addProjectMember(employee);
		} catch (OperationNotAllowedException e) {
			// errormessageholder.setErrorMessage(e.getMessage());
			errormessage = e.getMessage();
		}

	}



	@Given("^Wants to assign an employee with the info \"([^\"]*)\"$")
	public void wantsToAssignAnEmployeeWithTheInfo(String arg1) throws Exception {
		employee = new Employee(arg1);
	}

	@Then("^The user is given error message \"([^\"]*)\"$")
	public void theUserIsGivenErrorMessage(String arg1) throws Exception {
		try {
			assertEquals(arg1, errormessage);
		} catch (Exception e) {
			// nada
		}
	}

	// case 3

	@Given("^Employee with the info \"([^\"]*)\" exist$")
	public void employeeWithTheInfoExist(String employeeID) throws Exception {
		employee = new Employee(employeeID);

	}

	@Given("^Wishes to create a personal activity with the name \"([^\"]*)\" which starts \"([^\"]*)\" and ends \"([^\"]*)\"$")
	public void wishesToCreateAPersonalActivityWithTheNameWhichStartsAndEnds(String PaName, String startDate,
			String endDate) throws Exception {
		personalActivity = new PersonalActivity(PaName, startDate, endDate);

	}

	@When("^the the activity is created$")
	public void theTheActivityIsCreated() throws Exception {

		employee.addPersonalActivity(personalActivity);

	}

	@Then("^the activity is added to \"([^\"]*)\" personal activity list$")
	public void theActivityIsAddedToPersonalActivityList(String arg1) throws Exception {
		assertThat(employee.getPersonalActivities(), hasItem(personalActivity));
		

	}

	@When("^Failed to create personal activity with name \"([^\"]*)\", start \"([^\"]*)\", end \"([^\"]*)\"$")
	public void failedToCreatePersonalActivityWithNameStartEnd(String PaName, String startDate, String endDate)
			throws Exception {
		try {
			personalActivity = new PersonalActivity(PaName, startDate, endDate);
			employee.addPersonalActivity(personalActivity);
		} catch (OperationNotAllowedException e) {

			errormessage = e.getMessage();
		}
	}

	// case 4.

	@Given("^User is leader of project$")
	public void userIsLeaderOfProject() throws Exception {
		project.isprojectleader(true);

	}

	@Given("^a project member with the initials \"([^\"]*)\"$")
	public void aProjectMemberWithTheInitials(String ID) throws Exception {
		employee = new Employee(ID);
		project.addProjectMember(employee);

	}

	@When("^The project member is promoted$")
	public void theProjectMemberIsPromoted() throws Exception {
		try {
			project.setProjectLeader(employee);

		} catch (OperationNotAllowedException e) {

			errormessage = e.getMessage();
		}

	}

	@Then("^The employee with the set of \"([^\"]*)\" is changed to be the leader of the project$")
	public void theEmployeeWithTheSetOfIsChangedToBeTheLeaderOfTheProject(String employeeID) throws Exception {
		assertEquals(employeeID, project.getProjectLeaderID());

	}

	// case 5

	@Given("^User is an employee with initials \"([^\"]*)\" who is a part of the project and activity$")
	public void userIsAnEmployeeWithInitialsWhoIsAPartOfTheProjectAndActivity(String init) throws Exception {
		employee = new Employee(init);
		project.addProjectMember(employee);
		projectActivity.addProjectMember(employee);
	}

	@When("^the user registers (\\d+) hours time spent$")
	public void theUserRegistersHoursTimeSpent(double arg1) throws Exception {
		project.addProjectActivity(projectActivity);
		projectActivity.registerCumulatedTime(arg1);
	}

	@Then("^time spent (\\d+) is added to the activity and overall project$")
	public void timeSpentIsAddedToTheActivityAndOverallProject(double hours) throws Exception {
		double epsilon = 1; // epsilon describes the precision of the assertEquals.
		assertEquals(hours, project.getTotalHoursSpent(), epsilon);
		assertEquals(hours, projectActivity.getTimeSpent(), epsilon);
	}
	// case 6

	// case 7
	@When("^the employee is added to project$")
	public void theEmployeeIsAddedToProject() throws Exception {
		try {
			project.addProjectMember(employee);
		} catch (OperationNotAllowedException e) {
			errormessage = e.getMessage();
		}
	}

	@Then("^the employee with the initials \"([^\"]*)\" is in the project's employee list$")
	public void theEmployeeWithTheInitialsIsInTheProjectSEmployeeList(String arg1) throws Exception {
		assertThat(project.getEmployees(), hasItem(employee));
	}

	@Given("^\"([^\"]*)\" is already contained in the project$")
	public void isAlreadyContainedInTheProject(String arg1) throws Exception {
		project.addProjectMember(employee);
	}

	
	// case 8
	
	@Given("^project member \"([^\"]*)\"	is leader$")
	public void projectMemberIsLeader(String arg1) throws Exception {
		employeelead = new Employee(arg1);
		project.addProjectMember(employeelead);
		project.setProjectLeader(employeelead);
		project.UserIsProjectLeader = false;
	    
	    
	}
	

}
