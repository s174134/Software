package acceptance_tests;
//package project.tests;
//import static org.junit.Assert.assertFalse;
//import static org.junit.Assert.assertThat;
//import static org.junit.Assert.assertTrue;
//import static org.hamcrest.CoreMatchers.hasItem;
//import static org.junit.Assert.assertEquals;
//import cucumber.api.java.en.Given;
//import cucumber.api.java.en.Then;
//import cucumber.api.java.en.When;
//import project.app.Project;
//import project.app.ProjectActivity;
//import project.app.Employee;
//import project.app.OperationNotAllowedException;
//import project.tests.ErrorMessageHolder;
//public class AddingProjectMembertoAssignment {
//	private Project project;
//	private Employee employee;
//	private ProjectActivity activity;
//	private Employee employee2;
//	private ErrorMessageHolder errorMessage;
//	public AddingProjectMembertoAssignment(ProjectActivity activity, Project project, Employee employee, ErrorMessageHolder erorrMessage, Employee employee2) {
//this.activity=activity;
//this.project=project;
//this.employee=employee;
//this.errorMessage=erorrMessage;
//this.employee2=employee2;
//	}
//	@Given("^Project exists with the info \"([^\"]*\"$\"),\"([^\"]*\"$\"),\"([^\"]*\"$\")")
//	public void ProjectExists(String start, String end, String Name) throws Exception {
//		project = new Project(start, end, Name);
//	}	
//	@Given("^Project member exists with initials \"([^\"]*\"$\")")
//	public void MemberExists(String Init) throws Exception {
//		employee=new Employee(Init);
//		project.addProjectMember(employee);
//		employee.addProject(project);
//	}
//	@Given("^Activity exists on the project with the info \"([^\"]*\"$\"),\"([^\"]*\"$\"),\"([^\"]*\"$\")")
//	public void ActivityExists(String start, String end, String Name) throws Exception {
//		activity = new ProjectActivity(project,start, end, Name);
//	}
//	@When("^I add the probject member")
//	public void IAddMember() throws Exception {
//		activity.addProjectMember(employee);
//		employee.addProjectActivity(activity);	
//	}
//	@Then("^Member is on assignment member list")
//	public void MemberIsOnList(){
//		assertThat(employee.getProjectActivities(),hasItem(activity));
//		assertThat(activity.getAssignedEmployees(),hasItem(employee));
//	}
//
//	@Given("^Employee exists with initials \"([^\"]*\"$\")")
//	public void EmployeeExists(String Init) {
//		employee2=new Employee(Init);
//	}
//
//	@When("^Employee is added")
//	public void AddEmployee() throws Exception {
//		activity.addProjectMember(employee2);
//		employee2.addProjectActivity(activity);	
//	}
//	@Then("^Gets the error message \"([^\"]*\"$\")")
//	public void Employeegetserror(String errormessage) throws Exception {
//		assertEquals(errormessage, this.errorMessage.getErrorMessage());
//		}
//}
